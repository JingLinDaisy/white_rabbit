<?php

class WhiteRabbit2
{
    /**
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */

    
    public function findCashPayment($amount){

        $coins=array("1","2","5","10","20","50","100");
        $num=array(0,0,0,0,0,0,0);
     
        # iterate all available coin values
        for ($i=6; $i>-1; $i--) { 
            #dived amount by the coin value, get quotient 
            $quotient=floor($amount/$coins[$i]);
            #if quotient is zero, we can't use this coin
            if ($quotient==0){
                continue;
            }
            #if quotient isnot zero
            else{
                #quoient is the number of that coin we need
                $num[$i]=(int)$quotient;
                # we still have amount left, the process keeps going
                $amount-=$quotient*$coins[$i];
                }

        }
       
        return array_combine($coins,$num);
    }


}